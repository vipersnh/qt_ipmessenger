# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'start_widget.ui'
#
# Created by: PyQt4 UI code generator 4.12
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_start_widget(object):
    def setupUi(self, start_widget):
        start_widget.setObjectName(_fromUtf8("start_widget"))
        start_widget.resize(792, 234)
        start_widget.setMinimumSize(QtCore.QSize(651, 234))
        start_widget.setMaximumSize(QtCore.QSize(837, 257))
        self.verticalLayout_3 = QtGui.QVBoxLayout(start_widget)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.label_2 = QtGui.QLabel(start_widget)
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.verticalLayout_3.addWidget(self.label_2)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label_username = QtGui.QLabel(start_widget)
        self.label_username.setMinimumSize(QtCore.QSize(151, 20))
        self.label_username.setMaximumSize(QtCore.QSize(151, 20))
        self.label_username.setAlignment(QtCore.Qt.AlignCenter)
        self.label_username.setObjectName(_fromUtf8("label_username"))
        self.verticalLayout.addWidget(self.label_username)
        self.pushButton_select_target_directory = QtGui.QPushButton(start_widget)
        self.pushButton_select_target_directory.setMinimumSize(QtCore.QSize(171, 28))
        self.pushButton_select_target_directory.setMaximumSize(QtCore.QSize(171, 28))
        self.pushButton_select_target_directory.setObjectName(_fromUtf8("pushButton_select_target_directory"))
        self.verticalLayout.addWidget(self.pushButton_select_target_directory)
        self.horizontalLayout_2.addLayout(self.verticalLayout)
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.lineEdit_username = QtGui.QLineEdit(start_widget)
        self.lineEdit_username.setObjectName(_fromUtf8("lineEdit_username"))
        self.verticalLayout_2.addWidget(self.lineEdit_username)
        self.lineEdit_target_directory = QtGui.QLineEdit(start_widget)
        self.lineEdit_target_directory.setReadOnly(True)
        self.lineEdit_target_directory.setObjectName(_fromUtf8("lineEdit_target_directory"))
        self.verticalLayout_2.addWidget(self.lineEdit_target_directory)
        self.horizontalLayout_2.addLayout(self.verticalLayout_2)
        self.verticalLayout_3.addLayout(self.horizontalLayout_2)
        spacerItem1 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem1)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem2)
        self.pushButton_start_ipmessenger = QtGui.QPushButton(start_widget)
        self.pushButton_start_ipmessenger.setObjectName(_fromUtf8("pushButton_start_ipmessenger"))
        self.horizontalLayout.addWidget(self.pushButton_start_ipmessenger)
        spacerItem3 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem3)
        self.verticalLayout_3.addLayout(self.horizontalLayout)

        self.retranslateUi(start_widget)
        QtCore.QMetaObject.connectSlotsByName(start_widget)

    def retranslateUi(self, start_widget):
        start_widget.setWindowTitle(_translate("start_widget", "Form", None))
        self.label_2.setText(_translate("start_widget", "Please enter username and default file storage location before starting Qt IP Messenger", None))
        self.label_username.setText(_translate("start_widget", "Enter Username", None))
        self.pushButton_select_target_directory.setText(_translate("start_widget", "Select Target Directory", None))
        self.pushButton_start_ipmessenger.setText(_translate("start_widget", "Click to start IP Messenger", None))

