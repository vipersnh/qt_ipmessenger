import pickle
from pdb import set_trace
from PyQt4.QtCore import pyqtSignal, QObject, QTimer, QFile, QFileInfo, QIODevice
from PyQt4.QtNetwork import QTcpSocket, QHostAddress
from ctypes import Structure, c_char, c_uint, sizeof, string_at, byref, create_string_buffer, pointer, POINTER, cast
FILE_TRANSFER_SOCKET_PORT           = 10001
FILE_CHUNK_SIZE                     = 10*1024

FILE_TRANSFER_POLL_TIMER            = 1000  # ms 
FILE_TRANSFER_SOCKET_TIMEOUT_SEC    = 10    # Seconds
FILE_TRANSFER_ERROR_TIMER           = 10000 # ms

class file_transfer_hdr_t(Structure):
    _fields_ = [ 
                    ("filename", c_char*300),
                    ("filesize", c_uint) 
                ]

class file_send_t (QObject):

    signal_file_send_started = pyqtSignal(str)
    signal_file_send_status = pyqtSignal(str, object, object)
    signal_file_send_finished = pyqtSignal(str)
    signal_file_send_error = pyqtSignal(str)

    def __init__(this, filename, host_addr, parent=None):
        super(file_send_t, this).__init__(parent)
        this.is_connected = False
        this.filename = filename
        this.receiver_addr = host_addr
        this.timer = QTimer(this)
        this.timer.timeout.connect(this.slot_file_send_timer_poll)
        this.error_timer = QTimer(this)
        this.error_timer.timeout.connect(this.slot_file_send_error_timeout)
        this.send_f_obj = QFile(this.filename)
        this.send_f_obj.open(QIODevice.ReadOnly);
        this.socket = QTcpSocket(this)
        this.socket.bytesWritten.connect(this.slot_file_send_timer_poll)
        this.socket.connected.connect(this.slot_file_send_socket_connected)
        this.socket.disconnected.connect(this.slot_file_send_socket_disconnected)
        this.total_bytes = this.transferred_bytes = 0
        this.total_bytes = this.send_f_obj.size()

    def file_send_cleanup(this):
        this.socket.close()
        this.send_f_obj.close()
        this.timer.stop()
        this.error_timer.stop()
        del this.socket
        del this.send_f_obj
        del this.timer
        del this.error_timer

    def slot_file_send_start(this):
        this.error_timer.setInterval(FILE_TRANSFER_ERROR_TIMER)
        this.error_timer.start()
        this.socket.connectToHost(this.receiver_addr, FILE_TRANSFER_SOCKET_PORT)

    def slot_file_send_socket_connected(this):
        f_info = QFileInfo(this.filename)
        this.is_connected = True
        this.timer.start(FILE_TRANSFER_POLL_TIMER)
        this.error_timer.start()
        this.signal_file_send_started.emit(this.filename)
        f_name = f_info.baseName() + '.' + f_info.completeSuffix();
        header = file_transfer_hdr_t(filename=f_name.encode(), filesize=this.total_bytes)
        buf = string_at(byref(header), sizeof(file_transfer_hdr_t))
        this.socket.write(buf)

    def slot_file_send_socket_disconnected(this):
        this.is_connected = False
        this.timer.stop()
        this.error_timer.stop()
        this.signal_file_send_error.emit("Socket disconnected error")

    def slot_file_send_timer_poll(this):
        if this.socket.bytesToWrite()==0:
            # Reset error timer now since bytes were written to device
            this.error_timer.start()
            # All bytes written to device by now
            read = this.send_f_obj.read(FILE_CHUNK_SIZE)
            if len(read)==0:
                this.timer.stop()
                this.error_timer.stop()
                this.socket.disconnected.disconnect(this.slot_file_send_socket_disconnected)
                this.signal_file_send_finished.emit(this.filename)
            else:
                this.socket.write(read)
                this.transferred_bytes += len(read)
                this.signal_file_send_status.emit(this.filename,
                        this.transferred_bytes, this.total_bytes)

    def slot_file_send_stop(this):
        this.is_connected = False
        this.timer.stop()
        this.error_timer.stop()

    def slot_file_send_error_timeout(this):
        this.is_connected = False
        this.timer.stop()
        this.error_timer.stop()
        this.signal_file_send_error.emit("Connection timed out error")

class file_receive_t (QObject):
    
    signal_file_receive_started = pyqtSignal(str)
    signal_file_receive_status = pyqtSignal(str, object, object)
    signal_file_receive_finished = pyqtSignal(str)
    signal_file_receive_error = pyqtSignal(str)
    
    def __init__(this, socket, recv_directory, parent=None):
        super(file_receive_t, this).__init__(parent)
        this.filename = ""
        this.recv_f_obj = None
        this.sock = socket
        this.timer = QTimer(this)
        this.directory = recv_directory
        this.total_bytes = this.transferred_bytes = 0
        this.timer.timeout.connect(this.slot_file_receive_timer_poll)
        this.error_timer = QTimer(this)
        this.error_timer.timeout.connect(this.slot_file_receive_error_timeout)
        this.sock.disconnected.connect(this.slot_file_receive_socket_disconnected)

    def file_receive_cleanup(this):
        this.sock.close()
        this.recv_f_obj.close()
        this.timer.stop()
        this.error_timer.stop()
        del this.sock
        del this.recv_f_obj
        del this.timer
        del this.error_timer

    def slot_file_receive_start(this):
        this.error_timer.setInterval(FILE_TRANSFER_ERROR_TIMER)
        this.error_timer.start()
        this.timer.start(FILE_TRANSFER_POLL_TIMER)
        this.sock.readyRead.connect(this.slot_file_receive_timer_poll)

    def slot_file_receive_timer_poll(this):
        if this.recv_f_obj:
            if this.sock.bytesAvailable():
                this.error_timer.start()
                read = this.sock.readAll()
                this.recv_f_obj.write(read)
                this.transferred_bytes += len(read)
                this.signal_file_receive_status.emit(this.filename, this.transferred_bytes,
                        this.total_bytes)
                if this.transferred_bytes==this.total_bytes:
                    # File transfer finished
                    this.sock.disconnected.disconnect(this.slot_file_receive_socket_disconnected)
                    this.signal_file_receive_finished.emit(this.filename)
        else:
            if this.sock.bytesAvailable()>=sizeof(file_transfer_hdr_t):
                this.error_timer.start()
                buf = this.sock.read(sizeof(file_transfer_hdr_t))
                cstring = create_string_buffer(buf)
                header = cast(pointer(cstring), POINTER(file_transfer_hdr_t)).contents
                this.filename = header.filename.decode()
                this.signal_file_receive_started.emit(this.filename)
                this.recv_f_obj = QFile(this.directory + "/" + this.filename)
                this.recv_f_obj.open(QIODevice.WriteOnly | QIODevice.Text)
                this.transferred_bytes = 0
                this.total_bytes = header.filesize
            else:
                return

    def slot_file_receive_socket_disconnected(this):
        this.timer.stop()
        this.error_timer.stop()
        this.signal_file_receive_error.emit("Socket disconnected error")

    def slot_file_receive_stop(this):
        this.is_connected = False
        this.timer.stop()

    def slot_file_receive_error_timeout(this):
        this.timer.stop()
        this.error_timer.stop()
        this.signal_file_receive_error.emit("Connection timed out error")
