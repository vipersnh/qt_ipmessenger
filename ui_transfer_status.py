# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'transfer_status.ui'
#
# Created by: PyQt4 UI code generator 4.12
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_transfer_status(object):
    def setupUi(self, transfer_status):
        transfer_status.setObjectName(_fromUtf8("transfer_status"))
        transfer_status.resize(659, 152)
        transfer_status.setMinimumSize(QtCore.QSize(608, 120))
        transfer_status.setMaximumSize(QtCore.QSize(16777215, 152))
        self.verticalLayout_3 = QtGui.QVBoxLayout(transfer_status)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label_filename = QtGui.QLabel(transfer_status)
        self.label_filename.setMaximumSize(QtCore.QSize(131, 21))
        self.label_filename.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label_filename.setAlignment(QtCore.Qt.AlignCenter)
        self.label_filename.setObjectName(_fromUtf8("label_filename"))
        self.verticalLayout.addWidget(self.label_filename)
        self.label_status = QtGui.QLabel(transfer_status)
        self.label_status.setMaximumSize(QtCore.QSize(111, 21))
        self.label_status.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label_status.setAlignment(QtCore.Qt.AlignCenter)
        self.label_status.setObjectName(_fromUtf8("label_status"))
        self.verticalLayout.addWidget(self.label_status)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.lineedit_transfer_filename = QtGui.QLineEdit(transfer_status)
        self.lineedit_transfer_filename.setEnabled(True)
        self.lineedit_transfer_filename.setAlignment(QtCore.Qt.AlignCenter)
        self.lineedit_transfer_filename.setReadOnly(True)
        self.lineedit_transfer_filename.setObjectName(_fromUtf8("lineedit_transfer_filename"))
        self.verticalLayout_2.addWidget(self.lineedit_transfer_filename)
        self.progressBar = QtGui.QProgressBar(transfer_status)
        self.progressBar.setProperty("value", 24)
        self.progressBar.setObjectName(_fromUtf8("progressBar"))
        self.verticalLayout_2.addWidget(self.progressBar)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        self.verticalLayout_3.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.pushButton_cancel = QtGui.QPushButton(transfer_status)
        self.pushButton_cancel.setMaximumSize(QtCore.QSize(131, 31))
        self.pushButton_cancel.setObjectName(_fromUtf8("pushButton_cancel"))
        self.horizontalLayout_2.addWidget(self.pushButton_cancel)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.verticalLayout_3.addLayout(self.horizontalLayout_2)

        self.retranslateUi(transfer_status)
        QtCore.QMetaObject.connectSlotsByName(transfer_status)

    def retranslateUi(self, transfer_status):
        transfer_status.setWindowTitle(_translate("transfer_status", "Form", None))
        self.label_filename.setText(_translate("transfer_status", "Transfer FileName", None))
        self.label_status.setText(_translate("transfer_status", "Status", None))
        self.pushButton_cancel.setText(_translate("transfer_status", "Cancel", None))

