from re import search
import sys
import pickle
import platform
from subprocess import Popen, PIPE
from pdb import set_trace
from time import sleep
from PyQt4.QtCore import pyqtRemoveInputHook
from PyQt4.QtGui import QApplication
from PyQt4.QtCore import QObject, pyqtSignal, pyqtSlot, QThread, QTimer, QCoreApplication
from PyQt4.QtNetwork import QUdpSocket, QHostAddress
from collections import namedtuple

beacon_msg_t = namedtuple("beacon_msg_t", [ "random_id", "username" ]);

BEACON_RECEIVE_PORT = 5555
BEACON_MSG_INTERVAL_MS = 2000

class beacon_t (QObject):
    signal_beacon_message_received = pyqtSignal(beacon_msg_t, QHostAddress, int)

    def get_interface_broadcast_addresses(this):
        system_name = platform.system()
        if system_name=="Linux":
            process = Popen(['ifconfig'], stderr=PIPE, stdout=PIPE, stdin=PIPE)
            op_lines = process.stdout.readlines()
            if_name = None
            line_idx = 0
            while line_idx < len(op_lines):
                line = op_lines[line_idx].decode()
                if if_name==None and line!='\n':
                    if_name = line.split()[0]
                    line_idx += 1
                else:
                    if search('inet addr', line) and search('Bcast', line):
                        # Found IP address and Broadcast address, store this info
                        ip_addr = line.split()[1].split(':')[1]
                        bcast_addr = line.split()[2].split(':')[1]
                        this.interfaces[if_name] = (ip_addr, bcast_addr)
                        if_name = None
                        while (line_idx < len(op_lines)-1) and op_lines[line_idx].decode()!='\n':
                            line_idx += 1
                        if op_lines[line_idx].decode()=='\n':
                            line_idx += 1
                    else:
                        line_idx += 1
        elif system_name=="Windows":
            this.interfaces = dict()
            process = Popen(['ipconfig'], stdout=PIPE, stdin=PIPE, stderr=PIPE);
            op_lines = process.stdout.readlines()
            if_name = None
            ip_addr = bcast_addr = None
            line_idx = 4
            while line_idx < len(op_lines):
                line = op_lines[line_idx].decode()
                if if_name==None:
                    if_name = line.split(':')[0]
                    line_idx += 2
                else:
                    if search('IPv4 Address', line):
                        ip_addr = line.split()[-1]
                    elif search('Subnet Mask', line):
                        mask_addr_bytes = line.split()[-1].split('.')
                        ip_addr_bytes = ip_addr.split('.')
                        bcast_addr = []
                        for idx in range(4):
                            m0 = (~int(mask_addr_bytes[idx], 0)) & 0xFF
                            i0 = int(ip_addr_bytes[idx], 0)
                            b0 = str(m0 | i0)
                            bcast_addr.append(b0)
                        bcast_addr = ".".join(bcast_addr)
                    elif line=='':
                        if_name = ip_addr = bcast_addr = None
                        continue
                    if ip_addr and bcast_addr:
                        this.interfaces[if_name] = (ip_addr, bcast_addr)
                        if_name = ip_addr = bcast_addr = None
                        while (line_idx < len(op_lines)-1) and op_lines[line_idx].decode() != '':
                            line_idx += 1
                        if op_lines[line_idx].decode()=='':
                            line_idx += 1
                    else:
                        line_idx += 1

    def __init__(this, parent=None):
        super(beacon_t, this).__init__(parent)
        this.parent = parent
        this.interfaces = dict()
        this.get_interface_broadcast_addresses()

    def start_transmitter(this, random_id, username):
        this.transmit_message = beacon_msg_t(random_id, username)
        this.transmit_timer = QTimer(this)
        this.transmit_timer.timeout.connect(this.beacon_message_transmit_triggered)
        this.transmit_sock = QUdpSocket(this)
        this.transmit_timer.start(BEACON_MSG_INTERVAL_MS)

    def start_receiver(this):
        this.receive_sock = QUdpSocket(this)
        status = this.receive_sock.bind(QHostAddress.Any, BEACON_RECEIVE_PORT)
        if status==False:
            return False
        this.receive_sock.readyRead.connect(this.slot_beacon_message_received)
        return True


    def slot_beacon_message_received(this):
        while this.receive_sock.hasPendingDatagrams():
            (datagram, host, port) = this.receive_sock.readDatagram(1024)
            msg = pickle.loads(datagram)
            this.signal_beacon_message_received.emit(msg, host, port)

    def stop_beacon_receiver(this):
        this.receive_sock.close()
        del this.receive_sock

    def stop_beacon_transmitter(this):
        this.transmit_timer.stop()
        this.transmit_sock.close()
        del this.transmit_sock
        del this.transmit_message
        del this.transmit_timer

    def beacon_message_transmit_triggered(this):
        for key, value in this.interfaces.items():
            (_, bcast_addr) = value
            this.transmit_sock.writeDatagram(pickle.dumps(this.transmit_message), 
                    QHostAddress(bcast_addr), BEACON_RECEIVE_PORT)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    pyqtRemoveInputHook()
    thr = QThread()
    beacon = beacon_t(random_id = 0x123456789,
        username = "Sharankumar N Huggi")
    beacon.start()
    app.exec()
