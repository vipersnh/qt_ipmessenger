import os
import time
from beacon import beacon_t, BEACON_MSG_INTERVAL_MS
from file_transfer import *
from collections import OrderedDict
from PyQt4.QtCore import pyqtSignal, QObject, QSettings, qrand, qsrand, QTimer, QUrl
from PyQt4.QtGui import QWidget, QFileDialog, QDesktopServices, QMessageBox, QApplication, QPushButton, QFont, QVBoxLayout
from PyQt4.QtNetwork import QTcpServer, QTcpSocket, QHostAddress, QAbstractSocket
from ui_start_widget import Ui_start_widget
from ui_transfer_status import Ui_transfer_status
from ui_transfer_window import Ui_transfer_window

MODEL_RECENT_SENT_FILES_LIST_KEY        = "recent_sent_files"
MODEL_RECENT_RECEIVED_FILES_LIST_KEY    = "recent_received_files"
MODEL_USERNAME_KEY                      = "username"
MODEL_FILE_STORAGE_LOCATION_KEY         = "storage_directory"
MODEL_RECENT_FOLDER_USED                = "recent_folder_used"
MODEL_MAX_RECENT_FILES                  = 30
MODEL_USER_EXPIRY_TIMEOUT               = 4*BEACON_MSG_INTERVAL_MS

class user_t(QPushButton):

    signal_user_beacon_expired = pyqtSignal(object)
    signal_user_clicked = pyqtSignal(str)
    signal_user_send_file = pyqtSignal(str, str)

    def __init__(this, username, randomID, addr, timeout, parent=None):
        super(user_t, this).__init__(parent)
        font = QFont()
        font.setFamily("Courier 10 Pitch")
        font.setPointSize(12);
        this.setFont(font)
        this.setText(username + " @ " + addr.toString())
        this.setAcceptDrops(True)
        this.username = username
        this.addr = addr
        this.randomID = randomID
        this.timeout = timeout
        this.timer = QTimer(this)
        this.timer.setSingleShot(True)
        this.timer.timeout.connect(this.slot_user_beacon_timer_expired)
        this.clicked.connect(this.slot_user_clicked)
        this.timer.start(this.timeout)

    def dragEnterEvent(this, event):
        if event.mimeData().hasUrls():
            event.acceptProposedAction()
        else:
            event.ignore()

    def dropEvent(this, event):
        for url in event.mimeData().urls():
            fname = url.toLocalFile()
            if os.path.isfile(fname):
                this.signal_user_send_file.emit(this.username, fname)
            else:
                messageBox = QMessageBox(this)
                messageBox.setFixedSize(400, 200)
                messageBox.setWindowTitle("Information")
                messageBox.setText("Please select a file to send,\n{0} is a folder".format(fname))
                messageBox.show()
                messageBox.exec()
                

    def slot_user_clicked(this):
        this.signal_user_clicked.emit(this.username)

    def slot_user_beacon_timer_expired(this):
        this.signal_user_beacon_expired.emit(this)

    def slot_user_beacon_timer_reset(this):
        this.timer.stop()
        this.timer.start(this.timeout)

class gui_file_send_t (QWidget):

    signal_gui_file_transfer_finished = pyqtSignal(object, str)

    def __init__(this, filename, host_address, parent=None):
        super(gui_file_send_t, this).__init__(parent)
        this.ui = Ui_transfer_status()
        this.ui.setupUi(this)
        this.filename = filename
        this.file_send = file_send_t(filename, host_address)
        this.file_send.signal_file_send_started.connect(this.slot_gui_file_transfer_started)
        this.file_send.signal_file_send_status.connect(this.slot_gui_file_transfer_status)
        this.file_send.signal_file_send_finished.connect(this.slot_gui_file_transfer_finished)
        this.file_send.signal_file_send_error.connect(this.slot_gui_file_transfer_error)
        this.ui.pushButton_cancel.clicked.connect(this.slot_gui_file_transfer_cancelled)
        this.ui.lineedit_transfer_filename.setText("Waiting for connection, please wait")
        this.ui.progressBar.setValue(0)
        this.show()

    def gui_file_send_start(this):
        this.file_send.slot_file_send_start()

    def gui_file_send_cleanup(this):
        this.hide()
        this.file_send.file_send_cleanup()
        del this.file_send
        this.signal_gui_file_transfer_finished.emit(this, this.filename)

    def slot_gui_file_transfer_started(this, filename):
        this.filename = filename
        this.ui.lineedit_transfer_filename.setText(filename)

    def slot_gui_file_transfer_status(this, filename, num_bytes, total_bytes):
        this.ui.progressBar.setValue(num_bytes*100/total_bytes);

    def slot_gui_file_transfer_finished(this, filename):
        this.gui_file_send_cleanup();

    def slot_gui_file_transfer_cancelled(this):
        this.file_send.slot_file_send_stop();
        this.gui_file_send_cleanup();

    def slot_gui_file_transfer_error(this, message):
        messageBox = QMessageBox(this)
        messageBox.setFixedSize(600, 200)
        messageBox.setWindowTitle("Information")
        messageBox.setText(message)
        messageBox.setStandardButtons(QMessageBox.Ok)
        messageBox.show()
        messageBox.exec()
        this.slot_gui_file_transfer_cancelled()

class gui_file_receive_t (QWidget):

    signal_gui_file_transfer_finished = pyqtSignal(object, str)

    def __init__(this, sock, recv_directory, parent=None):
        super(gui_file_receive_t, this).__init__(parent)
        this.ui = Ui_transfer_status()
        this.ui.setupUi(this)
        this.file_receive = file_receive_t(sock, recv_directory)
        this.file_receive.signal_file_receive_started.connect(this.slot_gui_file_transfer_started)
        this.file_receive.signal_file_receive_status.connect(this.slot_gui_file_transfer_status)
        this.file_receive.signal_file_receive_finished.connect(this.slot_gui_file_transfer_finished)
        this.file_receive.signal_file_receive_error.connect(this.slot_gui_file_transfer_error)
        this.ui.pushButton_cancel.clicked.connect(this.slot_gui_file_transfer_cancelled)
        this.ui.lineedit_transfer_filename.setText("Waiting for connection, please wait")
        this.ui.progressBar.setValue(0)
        this.show()

    def gui_file_receive_start(this):
        this.file_receive.slot_file_receive_start()

    def gui_file_receive_cleanup(this):
        this.hide()
        this.file_receive.file_receive_cleanup()
        del this.file_receive
        this.signal_gui_file_transfer_finished.emit(this, this.filename)

    def slot_gui_file_transfer_started(this, filename):
        this.filename = filename
        this.ui.lineedit_transfer_filename.setText(filename)

    def slot_gui_file_transfer_status(this, filename, num_bytes, total_bytes):
        this.ui.progressBar.setValue(num_bytes*100/total_bytes);

    def slot_gui_file_transfer_finished(this, filename):
        this.gui_file_receive_cleanup();

    def slot_gui_file_transfer_cancelled(this):
        this.file_receive.slot_file_receive_stop();
        this.gui_file_receive_cleanup();

    def slot_gui_file_transfer_error(this, message):
        messageBox = QMessageBox(this)
        messageBox.setFixedSize(600, 200)
        messageBox.setWindowTitle("Information")
        messageBox.setText(message)
        messageBox.setStandardButtons(QMessageBox.Ok)
        messageBox.show()
        messageBox.exec()
        this.slot_gui_file_transfer_cancelled()

class gui_model_t (QObject):

    signal_gui_model_terminate = pyqtSignal()

    def __init__(this, parent=None):
        super(gui_model_t, this).__init__(parent)

        this.beacon = None
        this.server = None
        this.obj_refs = list()

        this.storage_directory = this.username = None
        this.allusers = list()
        this.userslist = OrderedDict()

        this.transfer_window = None
        this.username = None
        this.random_id = None

        this.settings = QSettings("vipersnh", "qt_messenger")
        this.widget = QWidget()
        this.start_widget = Ui_start_widget()
        this.start_widget.setupUi(this.widget)

        username = this.settings.value(MODEL_USERNAME_KEY)
        storage_directory = this.settings.value(MODEL_FILE_STORAGE_LOCATION_KEY)

        this.start_widget.lineEdit_username.setText(username)
        this.start_widget.lineEdit_target_directory.setText(storage_directory)

        this.start_widget.pushButton_select_target_directory.clicked.connect(
                this.slot_gui_model_select_target_directory_clicked)
        this.start_widget.pushButton_start_ipmessenger.clicked.connect(
                this.slot_gui_model_start_ip_messenger_clicked)

        this.widget.setWindowTitle("IP Messenger")
        this.widget.show()

        if (0):
            this.settings.setValue(MODEL_RECENT_RECEIVED_FILES_LIST_KEY, [])
            this.settings.setValue(MODEL_RECENT_SENT_FILES_LIST_KEY, [])
            this.settings.sync()

    def slot_gui_model_start_ip_messenger_clicked(this):
        qsrand(time.time())
        this.random_id = (qrand()<<32) | qrand()
        
        this.username = this.start_widget.lineEdit_username.text()
        this.storage_directory = this.start_widget.lineEdit_target_directory.text()

        if len(this.username)==0:
            messageBox = QMessageBox()
            messageBox.setFixedSize(600,200)
            messageBox.critical(this.widget, "Error !!!","Please enter a valid username. Exiting application now")
            QApplication.exit(-1)
            return
        
        if os.path.isdir(this.storage_directory)==0:
            messageBox = QMessageBox()
            messageBox.setFixedSize(600,200)
            messageBox.critical(this.widget, "Error !!!","Please enter a valid target directory to store files. Exiting application now")
            QApplication.exit(-1)
            return

        this.beacon = beacon_t()
        this.beacon.signal_beacon_message_received.connect(this.slot_gui_model_beacon_message_received)
        status = this.beacon.start_receiver()
        if status==False:
            messageBox = QMessageBox()
            messageBox.setFixedSize(600,200)
            messageBox.critical(this.widget, "Warning !!!","An instance of IP Messenger may already be running !")
            QApplication.exit(-1)
            return

        # Wait for 4*BEACON_MSG_INTERVAL_MS to check for username not being used by others
        messageBox = QMessageBox(this.widget)
        messageBox.setFixedSize(600, 200)
        messageBox.setWindowTitle("Information")
        messageBox.setText("Please wait. Checking whether username used is unique and does not conflict with others")
        messageBox.setStandardButtons(QMessageBox.NoButton)
        messageBox.show()
        QTimer.singleShot(4*BEACON_MSG_INTERVAL_MS, messageBox.accept)
        messageBox.exec()

        if this.username in this.allusers:
            messageBox = QMessageBox()
            messageBox.setFixedSize(600,200)
            messageBox.critical(this.widget, "Error !!!","An user of IP Messenger already has the username {0}. Please use another name".format(this.username))
            QApplication.exit(-1)
            return

        this.beacon.start_transmitter(this.random_id, this.username)

        this.widget.hide()
        del this.start_widget
        del this.widget

        this.widget = QWidget()
        this.transfer_window = Ui_transfer_window()
        this.transfer_window.setupUi(this.widget)
        this.widget.setWindowTitle("IP Messenger @ {0} (Saving to : {1}".format(this.username, this.storage_directory));
        this.transfer_window.listWidget_recent_sent_files.itemDoubleClicked.connect(
                this.slot_gui_model_open_file)
        this.transfer_window.listWidget_recent_received_files.itemDoubleClicked.connect(
                this.slot_gui_model_open_file)
        this.usersListLayout = QVBoxLayout(this.transfer_window.usersScrollArea)
        this.widget.show()

        this.server = QTcpServer()
        this.server.newConnection.connect(this.slot_gui_model_file_receive_requested)
        this.server.listen(QHostAddress.Any, FILE_TRANSFER_SOCKET_PORT)

        this.settings.setValue(MODEL_USERNAME_KEY, this.username)
        this.settings.setValue(MODEL_FILE_STORAGE_LOCATION_KEY, this.storage_directory)
        this.settings.sync()

        files_list = this.settings.value(MODEL_RECENT_RECEIVED_FILES_LIST_KEY)
        if files_list:
            for f_name in files_list:
                this.transfer_window.listWidget_recent_received_files.addItem(f_name)
            this.recent_received_files = files_list
        else:
            this.recent_received_files = list()

        files_list = this.settings.value(MODEL_RECENT_SENT_FILES_LIST_KEY)
        if files_list:
            for f_name in files_list:
                this.transfer_window.listWidget_recent_sent_files.addItem(f_name)
            this.recent_sent_files = files_list
        else:
            this.recent_sent_files = list()



    def slot_gui_model_select_target_directory_clicked(this):
        if this.storage_directory:
            prev_dir = this.storage_directory
        else:
            prev_dir = "/home"
        dir_name = QFileDialog.getExistingDirectory(this.widget, "Open Directory",
                prev_dir, QFileDialog.ShowDirsOnly | QFileDialog.DontResolveSymlinks)
        this.start_widget.lineEdit_target_directory.setText(dir_name)

    def slot_gui_model_beacon_message_received(this, msg, addr, port):
        if msg.random_id != this.random_id : # Check if this 
            if msg.username not in this.allusers:
                this.allusers.append(msg.username)
            if this.transfer_window:
                if msg.username not in this.userslist.keys():
                    user = user_t(msg.username, msg.random_id, addr, 
                            MODEL_USER_EXPIRY_TIMEOUT)
                    user.signal_user_beacon_expired.connect(this.slot_gui_model_user_expired)
                    user.signal_user_clicked.connect(this.slot_gui_model_user_clicked)
                    user.signal_user_send_file.connect(this.slot_gui_model_file_send_requested)
                    this.userslist[msg.username] = user
                    this.usersListLayout.addWidget(user)
                else:
                    if msg.random_id != this.userslist[msg.username].randomID:
                        messageBox = QMessageBox()
                        messageBox.setFixedSize(600,200)
                        messageBox.critical(this.widget, "Error !!!","Multiple instances of IP Messenger have same username {0}".format(msg.username))
                        QApplication.exit(-1)
                        return
                    else:
                        this.userslist[msg.username].slot_user_beacon_timer_reset()

    def slot_gui_model_user_clicked(this, username):
        recent_folder = this.settings.value(MODEL_RECENT_FOLDER_USED, None)
        if recent_folder==None:
            recent_folder = "/home"
            
        filename = QFileDialog.getOpenFileName(this.widget, "Select file for transferring", recent_folder, "")
        if len(filename):
            recent_folder = os.path.dirname(filename)
            this.slot_gui_model_file_send_requested(username, filename)
            this.settings.setValue(MODEL_RECENT_FOLDER_USED, recent_folder)
            this.settings.sync()

    def slot_gui_model_file_send_requested(this, username, filename):
        addr = this.userslist[username].addr
        gui_file_send = gui_file_send_t(filename, addr)
        gui_file_send.signal_gui_file_transfer_finished.connect(
                this.slot_gui_model_file_send_finished)
        gui_file_send.gui_file_send_start()
        this.obj_refs.append(gui_file_send)

    def slot_gui_model_file_receive_requested(this):
        sock = this.server.nextPendingConnection()
        assert sock
        gui_file_receive = gui_file_receive_t(sock, this.storage_directory)
        gui_file_receive.signal_gui_file_transfer_finished.connect(
                this.slot_gui_model_file_receive_finished)
        gui_file_receive.gui_file_receive_start();
        this.obj_refs.append(gui_file_receive)


    def slot_gui_model_file_send_finished(this, gui_file_send_ref, filename):
        if filename not in this.recent_sent_files:
            if len(this.recent_sent_files)>MODEL_MAX_RECENT_FILES:
                del this.recent_sent_files[-1]
            this.recent_sent_files.insert(0, filename)
            this.transfer_window.listWidget_recent_sent_files.insertItem(0, filename)
            this.settings.setValue(MODEL_RECENT_SENT_FILES_LIST_KEY, this.recent_sent_files)
            this.settings.sync()
        this.obj_refs.remove(gui_file_send_ref)
        del gui_file_send_ref

    def slot_gui_model_file_receive_finished(this, gui_file_recv_ref, filename):
        if filename not in this.recent_received_files:
            if len(this.recent_received_files)>MODEL_MAX_RECENT_FILES:
                del this.recent_received_files[-1]
            this.recent_received_files.insert(0, filename)
            this.transfer_window.listWidget_recent_received_files.insertItem(0, filename)
            this.settings.setValue(MODEL_RECENT_RECEIVED_FILES_LIST_KEY, this.recent_received_files)
            this.settings.sync()
        this.obj_refs.remove(gui_file_recv_ref)
        del gui_file_recv_ref

    def slot_gui_model_open_file(this, item):
        filename = item.text()
        QDesktopServices.openUrl(QUrl(filename))

    def slot_gui_model_user_expired(this, user_ref):
        user_ref.setParent(None)
        del this.userslist[user_ref.username]
        del user_ref

    def slot_gui_model_closed(this):
        if this.beacon:
            this.beacon.stop_beacon_receiver()
            this.beacon.stop_beacon_transmitter()
            del this.beacon
        if this.server:
            this.server.close()
            del this.server
        this.signal_gui_model_terminate.emit()
