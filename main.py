import sys
from PyQt4.QtGui import QApplication
from PyQt4.QtCore import pyqtRemoveInputHook
from gui_model import gui_model_t


if __name__ == "__main__":
    app = QApplication(sys.argv)
    pyqtRemoveInputHook()
    gui_model = gui_model_t()
    app.lastWindowClosed.connect(gui_model.slot_gui_model_closed)
    gui_model.signal_gui_model_terminate.connect(app.quit)
    app.exec()
